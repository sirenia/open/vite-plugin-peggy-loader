# [Peggy](https://github.com/peggyjs/peggy) loader for [Vite](https://github.com/vitejs/vite)

[![npm version](https://img.shields.io/npm/v/vite-plugin-peggy-loader.svg?style=flat-square)](https://www.npmjs.com/package/vite-plugin-peggy-loader)
[![npm downloads](https://img.shields.io/npm/dm/vite-plugin-peggy-loader.svg?style=flat-square)](https://www.npmjs.com/package/vite-plugin-peggy-loader)

## Install

code based on [peggy-loader for webpack](https://github.com/sireniaeu/peggy-loader).

`npm install vite-plugin-peggy-loader`

The pegjs-loader requires [peggy](https://github.com/peggyjs/peggy)
as [`peerDependency`](https://docs.npmjs.com/files/package.json#peerdependencies). Thus you are able to specify the required version accurately.

## Usage

Apply the loader by adjusting your `vite.config` so `.pegjs` is automatically transformed to `.js`

``` js
import peggyLoader from "vite-plugin-peggy-loader";

export default defineConfig({
    plugins: [peggyLoader()]
});
```

Then to use the compiled version import it like: 

```js
import * as grammer from './grammer.pegjs'
```

### PEG.js options

You can pass options to PEG.js in the config, below is the supported options:

* `allowedStartRules` - The rules the built parser will be allowed to start
  parsing from (default: the first rule in the grammar).

* `cache` — If `true`, makes the parser cache results, avoiding exponential
  parsing time in pathological cases but making the parser slower (default:
  `false`).

* `dependencies` - Parser dependencies, the value is an object which maps variables used to access the
  dependencies in the parser to module IDs used to load them (default: `{}`).

* `optimize` - Whether to optimize the built parser either for `speed` or
  `size` (default: `speed`).

* `trace` - If `true`, the tracing support in the built parser is enabled
  (default: `false`).

``` js
import peggyLoader from "peggy-loader";

export default defineConfig({
    plugins: [
        peggyLoader({
            cache: false,
            optimizeParser: 'speed',
            trace: false,
            dependencies: {},
            allowedStartRules: []
        })
    ]
});
```

## Change Log

This project adheres to [Semantic Versioning](http://semver.org/). 

## Thanks

* [Victor Homyakov](https://github.com/victor-homyakov) for the propagation of the `cache` option.
* [VladimirTechMan](https://github.com/VladimirTechMan) for the propagation of the `optimize` option and updating things to be compatible with PEG.js 0.10.0.
* [ragtime](https://github.com/ragtime) for the propagation of the `allowedStartRules` and `trace` options.
* [Jan Varwig](https://github.com/janv) for the Webpack 2 compatibility fix.
* [retorquere](https://github.com/retorquere) for the propagation of the `dependencies` option.

## License

MIT (http://www.opensource.org/licenses/mit-license.php)
